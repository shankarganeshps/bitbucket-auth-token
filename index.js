var homeDir = require('home-dir');
var jsonfile = require('jsonfile');
var request = require('superagent');
var RSVP = require('rsvp');
var _ = require('lodash');

function getAccessToken(opts) {

  // appName, consumerKey, consumerSecret, credentialsProvider, forceCredentials, domain
  var configPath = homeDir("/." + opts.appName);
  var config;
  try {
    config = jsonfile.readFileSync(configPath);
  } catch (e) {
    config = {};
  }
  opts = _.extend({}, {
    configPath: configPath,
    config: config,
    domain: "bitbucket.org"
  }, opts);

  if (config.refreshToken && !opts.forceCredentials) {
    opts.refreshToken = config.refreshToken;
    return getTokens(opts);
  } else if (opts.credentialsProvider) {
    return opts.credentialsProvider().then(function(credentials) {
      opts = _.extend(opts, credentials);
      return getTokens(opts);
    });
  } else {
    throw 'opts must specify a credentialsResolver';
  }
}

function getTokens(opts) {

  var payload;
  var errorMessageOn401 = 'Authentication failed!';

  if (opts.username) {
    payload = {
      grant_type: 'password',
      username: opts.username,
      password: opts.password
    };
    errorMessageOn401 += ' Bad username/password?';
  } else if (opts.refreshToken) {
    payload = {
      grant_type: 'refresh_token',
      refresh_token: opts.refreshToken
    };
    errorMessageOn401 += ' Bad refresh token?';
  } else {
    throw 'opts must specify either username and password, or refreshToken';
  }

  return new RSVP.Promise(function (resolve, reject) {
    request
      .post('https://' + opts.domain + '/site/oauth2/access_token')
      .auth(opts.consumerKey, opts.consumerSecret)
      .accept('application/json')
      .type('form')
      .send(payload)
      .end(function (err, res) {
        if (res && res.ok) {
          var newConfig = _.extend(opts.config, {
            refreshToken: res.body.refresh_token
          });
          jsonfile.writeFile(opts.configPath, newConfig, {
            mode: 0600
          }, function() {
            // log a message if we're using the password flow to retrieve a token
            if (opts.username) {
              opts.logger('storing auth token in ' + opts.configPath);
            }
          });
          resolve(res.body.access_token);
        } else {
          var errorMessage;
          if (res && res.status === 401) {
            errorMessage = errorMessageOn401;
          } else if (err) {
            errorMessage = err;
          } else {
            errorMessage = res.text;
          }
          reject(errorMessage);
        }
      });
  });
}

module.exports = {
  getAccessToken: getAccessToken
};
